#!/usr/bin/env bash

if [[ "$1" = total ]]; then
	$(bc <<< "(2^64)-1")
elif ! [[ "$1" -gt 0 && "$1" -lt 65 ]]; then
	echo "Error: invalid input"
	exit 1
else
	$(bc <<< "2^($1-1)")
fi
