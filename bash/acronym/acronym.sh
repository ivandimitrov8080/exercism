#!/usr/bin/env bash

input=$(echo "$1" | tr '-' ' ' | tr '_' ' ' | tr '*' ' ')

IFS=' '

read -ra words <<< "$input";
result=""
for word in "${words[@]}"; do
    result+="${word:0:1}"
done
acronym=$(echo "${result}" | tr '[:lower:]' '[:upper:]')

echo "$acronym"
