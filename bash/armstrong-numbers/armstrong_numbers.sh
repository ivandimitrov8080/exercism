#!/usr/bin/env bash

num=$1
sum=0
for (( i=0; i<${#num}; i++ )); do
    digit=$((${num:$i:1}**${#num}))
    ((sum+=digit))
done

[[ "$sum" = "$num" ]] && echo true || echo false
