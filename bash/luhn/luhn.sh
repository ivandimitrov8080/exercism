#!/usr/bin/env bash

input=$( sed "s/ *//g" <<< "$1" )
[[ ! $input =~ ^[0-9]{2,}$ ]] && echo false && exit 0

luhndigit() {
	doubled=$(( $1 * 2 ))
	[[ "$doubled" -gt 9 ]] && (( doubled-=9 ))
	echo $doubled
}

luhnnumber() {
	input=$1
	output=""
	tracker=1
	for (( i=${#input} - 1; i>-1; i-- )); do
		digit=${input:$i:1}
		[[  $(expr $tracker % 2) -eq 0 ]] && output+=$( luhndigit $digit ) || output+=$digit
		(( tracker++ ))
	done
	echo $output
}

luhnstring=$( luhnnumber $input )
sum=0
for (( i=0; i<${#luhnstring}; i++ )); do
	(( sum+=${luhnstring:$i:1} ))
done

[[ $(expr $sum % 10) -eq 0 ]] && echo true || echo false

