#!/usr/bin/env bash

job="$1"
input=$( tr '[[:upper:]]' '[[:lower:]]' <<< $( tr -d '[[:punct:]]' <<< "$2") )
input=$( tr -d '[[:space:]]' <<< "$input" )

reversechar() {
	char="$1"
	[[ "$char" =~ [0-9] ]] && echo $char && return
	alphabet="abcdefghijklmnopqrstuvwxyz"
	reversed_alphabet="zyxwvutsrqponmlkjihgfedcba"
	alphabet_after_char=${alphabet#*$char}
	index=$(( ${#alphabet} - ${#alphabet_after_char} - ${#char} ))
	echo ${reversed_alphabet:$index:${#char}}
}

encode() {
	text="$1"
	output=""
	for (( i=0; i<${#text}; i++ )); do
		current_char=${text:$i:1}
		reversed_char=$( reversechar "$current_char" )
		output+=$reversed_char
		[[ "$(( (i + 1) % 5 ))" -eq 0 ]] && output+=" "
	done
	echo $output
}

decode() {
	text="$1"
	output=""
	for (( i=0; i<${#text}; i++ )); do
		current_char=${text:$i:1}
		reversed_char=$( reversechar "$current_char" )
		output+=$reversed_char
		[[ "$(( (i + 1) % 5 ))" -eq 0 ]]
	done
	echo $output
}

[[ "$job" = encode ]] && encode $input || decode $input

