#!/usr/bin/env bash

dna1=$1
dna2=$2

handleLengthError() {
    echo "strands must be of equal length"
    exit 1
}

handleInvalidInputError() {
    echo "Usage: hamming.sh <string1> <string2>"
    exit 1
}

[[ "$#" = 2 ]] || handleInvalidInputError

[[ "${#dna1}" = "${#dna2}" ]] || handleLengthError
count=0

for (( i=0; i<${#dna1}; i++ )); do
    [[ "${dna1:$i:1}" = "${dna2:$i:1}" ]] || ((count+=1))
done

echo $count
