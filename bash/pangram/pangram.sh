#!/usr/bin/env bash

input=$( echo "$1" | tr '[:upper:]' '[:lower:]' )

notpangram() {
    echo false
    exit 0
}

for letter in {a..z}; do
    [[ $input == *"$letter"* ]] || notpangram
done

echo true
