#!/usr/bin/env bash

set -f

question() {
    echo "Sure."
}

yell() {
    echo "Whoa, chill out!"
}

yellquestion() {
    echo "Calm down, I know what I'm doing!"
}

address() {
    echo "Fine. Be that way!"
}

default() {
    echo "Whatever."
}

determinecase() {
    echo $( echo "$1" | awk  "{a=0}/^${2}$/{a=1}a" )
}

[[ "$1" =~ ^[^a-z]*[A-Z]+[^a-z]*\?+[[:blank:]]*$ ]] && yellquestion && exit 0;
[[ "$1" =~ ^.*\?+[[:blank:]]*$ ]] && question && exit 0;
[[ "$1" =~ ^[^a-z]*[A-Z]+[^a-z]*$ ]] && yell && exit 0;
[[ "$1" =~ ^[^a-zA-Z0-9]*[[:blank:]]*$ ]] && address && exit 0;
[[ "$1" =~ ^.*$ ]] && default
