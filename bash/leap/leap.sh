#!/usr/bin/env bash

error() {
	echo "Usage: leap.sh <year>"
	exit 1
}

success() {
	echo true
	exit 0
}

failure() {
	echo false
	exit 0
}

[[ "$#" -ne 1 ]] && error

[[ "$1" =~ ^[0-9]+$ ]] || error

[[ $(( $1 % 400 )) -eq 0 ]] && success

[[ $(( $1 % 100 )) -eq 0 ]] && failure

[[ $(( $1 % 4 )) -eq 0 ]] && success

failure

