#!/usr/bin/env bash
onep=("A" "E" "I" "O" "U" "L" "N" "R" "S" "T")
twop=("D" "G")
threep=("B" "C" "M" "P")
fourp=("F" "H" "V" "W" "Y")
fivep=("K")
eightp=("J" "X")
tenp=("Q" "Z")

ans=0
for (( i=0; i<${#1}; i++)); do
    letter="$( echo "${1:$i:1}" | tr '[[:lower:]]' '[[:upper:]]' )"
    [[ "${onep[*]}" =~ "$letter" ]] && (( ans+=1 ))
    [[ "${twop[*]}" =~ "$letter" ]] && (( ans+=2 ))
    [[ "${threep[*]}" =~ "$letter" ]] && (( ans+=3 ))
    [[ "${fourp[*]}" =~ "$letter" ]] && (( ans+=4 ))
    [[ "${fivep[*]}" =~ "$letter" ]] && (( ans+=5 ))
    [[ "${eightp[*]}" =~ "$letter" ]] && (( ans+=8 ))
    [[ "${tenp[*]}" =~ "$letter" ]] && (( ans+=10 ))
done

echo $ans